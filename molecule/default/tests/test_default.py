import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_files(host):
    files = [
        "/opt/jolokia_exporter/jolokia_exporter",
        "/lib/systemd/system/jolokia_exporter.service"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_user(host):
    assert host.group("jolokia_exporter").exists
    assert "jolokia_exporter" in host.user("jolokia_exporter").groups
    assert host.user("jolokia_exporter").shell == "/sbin/nologin"
    assert host.user("jolokia_exporter").home == "/opt/jolokia_exporter"


# def test_service(host):
#     s = host.service("trickster")
#     assert s.is_enabled
#     assert s.is_running


# def test_socket(host):
#     sockets = [
#         "tcp://127.0.0.1:8082"
#     ]
#     for socket in sockets:
#         s = host.socket(socket)
#         assert s.is_listening
